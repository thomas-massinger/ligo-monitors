#!/bin/bash
#
# Run the gwdetchar-software-saturations code on a daily stride

export MPLCONFIGDIR=${HOME}/.matplotlib
. ~/.bash_profile
if [ "$USER" = "detchar" ]; then
    unset X509_USER_PROXY
fi
set -e

PYTHON_VERSION=$(
    python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')
GWPYSOFT_VIRTUAL_ENV="/home/detchar/opt/gwpysoft-${PYTHON_VERSION}"
. ${GWPYSOFT_VIRTUAL_ENV}/bin/activate
echo "-- Environment set"

here_=$(cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))" && pwd)
. ${here_}/functions
echo "-- Loaded local functions"

# get run date
gpsstart=`gps_start_yesterday`
gpsend=`gps_start_today`
date_=`yyyymmdd ${gpsstart}`
duration=$((gpsend-gpsstart))
echo "-- Identified run date as ${date_}"

# set up output directory
htmlbase=${HOME}/public_html/software-saturations/
outdir=${htmlbase}/day/${date_}
mkdir -p ${outdir}
cd ${outdir}
echo "-- Moved to output directory: `pwd`"

# set up channels to skip (funky bash to allow adding skips as newlines)
skip=`echo "
ALS-
PSL-ISS_INLOOP_PD_SELECT_CALI
CAL-CS_DARM_FE_ETMY_M0_LOCK_L
ISI-GND_BRS_ETMX_RY
" | tr '\n' ' '`

if [[ "`hostname -f`" == *"ligo-la"* ]]; then
    export IFO="L1"
elif [[ "`hostname -f`" == *"ligo-wa"* ]]; then
    export IFO="H1"
else
    echo "Cannot determine IFO" 1>&2 && false
fi

# run the code
nagios_status 0 "Daily overflows analysis for ${date_} is running" 10000 "Daily overflows analysis for ${date_} is taking too long" > ${htmlbase}/nagios.json
set +e

cmd="gwdetchar-software-saturations $gpsstart $gpsend --ifo ${IFO} --frametype ${IFO}_R --html index.html --plot --nproc 4 --state-flag ${IFO}:DMT-UP:1 --pad-state-end 1 --skip ${skip}"
echo "$ $cmd" && eval $cmd 2> /tmp/daily-software-saturations-$USER.err

EXITCODE="$?"
deactivate

# write JSON output
TIMER=100000
TIMEOUT="Daily software saturations analysis has not run since ${date_}"
if [ "$EXITCODE" -eq 0 ]; then
    MESSAGE="Daily software saturations analysis for ${date_} complete"
    nexit=0
else
    MESSAGE=`echo -n "Daily software saturations analysis for ${date_} failed with exitcode $EXITCODE" && sed ':a;N;$!ba;s/\n/\\\n/g' /tmp/daily-software-saturations-$USER.err | tr '"' "'"`
    nexit=2
fi

nagios_status $nexit "$MESSAGE" $TIMER "$TIMEOUT" > ${htmlbase}/nagios.json
exit ${EXITCODE}
